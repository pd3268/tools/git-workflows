FROM alpine/git:v2.36.2

ARG ARGO_VERSION=3.3.9
ARG YQ_VERSION=4.27.5

RUN  apk add --no-cache --virtual=.build-dependencies \
      curl \
    ; \
    apk add bash;\
    \
    # Install argo cli
    curl -sLO https://github.com/argoproj/argo-workflows/releases/download/v${ARGO_VERSION}/argo-linux-amd64.gz && \
    gunzip argo-linux-amd64.gz && \
    chmod +x argo-linux-amd64 && \
    mv ./argo-linux-amd64 /usr/local/bin/argo \
    ;\
    # Install yq
    curl -sLO https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64.tar.gz && \
    tar xf yq_linux_amd64.tar.gz && \
    mv yq_linux_amd64 /usr/local/bin/yq; \
    rm -rf *; \
    \
    # Remove unwanted software
    apk del .build-dependencies && \
    rm -rf /var/cache/apk/* /tmp/* /var/tmp/*

WORKDIR /workflows
ENTRYPOINT [ "/bin/bash" ]